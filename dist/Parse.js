function getTypeObj(type, typeArgs) {
    const o = {
        type,
        typeArgs
    };
    if (!typeArgs) {
        delete o.typeArgs;
    }
    return o;
}
function parse(input) {
    let baseTypes = ["int", "string", "bool"];
    let baseType = baseTypes.find(type => type === input);
    if (baseType) {
        return getTypeObj(baseType);
    }
    let arrayType = "Array";
    if (input.includes(arrayType)) {
        const f = input.indexOf(arrayType) + arrayType.length + 1;
        return getTypeObj(arrayType, parse(input.slice(f, input.length - 1)));
    }
    return null;
}
