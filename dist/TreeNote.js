function findNodeById(root, id) {
    if (root.id === id) {
        return root;
    }
    if (!root.children) {
        return null;
    }
    let note = null;
    for (let item of root.children) {
        if ((note = findNodeById(item, id))) {
            return note;
        }
    }
    return note;
}
