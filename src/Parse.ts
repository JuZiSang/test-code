function getTypeObj(type: string, typeArgs?) {
  const o = {
    type,
    typeArgs
  };
  if (!typeArgs) {
    delete o.typeArgs;
  }
  return o;
}

function parse(input: string) {
  // 基础类型
  let baseTypes = ["int", "string", "bool"];
  let baseType = baseTypes.find(type => type === input);
  if (baseType) {
    return getTypeObj(baseType);
  }
  // 泛型数组
  let arrayType = "Array";
  if (input.includes(arrayType)) {
    const f = input.indexOf(arrayType) + arrayType.length + 1;
    // 取 Array<?> 中间的值，递归
    return getTypeObj(arrayType, parse(input.slice(f, input.length - 1)));
  }
  return null;
}
