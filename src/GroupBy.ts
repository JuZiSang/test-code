interface Student {
  name: string;
  score: number;
}

function getGrade(score) {
  return score < 60 ? "C" : score < 80 ? "B" : "A";
}

function groupBy(students: Array<Student>) {
  students.sort((a, b) => (a.score > b.score ? -1 : 1));
  let groups: any = {};
  for (let item of students) {
    const grade = getGrade(item.score);
    if (groups[grade]) {
      groups[grade].push(item);
    } else {
      groups[grade] = [item];
    }
  }
  return groups;
}
